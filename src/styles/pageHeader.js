export const PAGE_HEADER_STYLE = {
    borderBottom: "1px solid rgb(232, 232, 232)",
    background: "white",
    padding: "12px 24px"
};

export const PAGE_HEADER_TITLE_STYLE = {
    fontSize: "1rem",
    lineHeight: "22px",
    margin: "5px 0"
};

export const PAGE_HEADER_SUBTITLE_STYLE = {
    lineHeight: "23px"
};
