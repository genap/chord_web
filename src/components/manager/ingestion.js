export const FORM_LABEL_COL = {md: {span: 24}, lg: {span: 4}, xl: {span: 6}, xxl: {span: 8}};
export const FORM_WRAPPER_COL = {md: {span: 24}, lg: {span: 16}, xl: {span: 12}, xxl: {span: 8}};
export const FORM_BUTTON_COL = {
    md: {span: 24},
    lg: {offset: 4, span: 16},
    xl: {offset: 6, span: 12},
    xxl: {offset: 8, span: 8}
};
